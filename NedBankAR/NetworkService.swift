//
//  NetworkService.swift
//  NedBankAR
//
//  Created by Rupender Rana on 25/07/19.
//  Copyright © 2019 CompanyName. All rights reserved.
//

import UIKit

class Property: Codable {
    let price, bedrooms, sqftLiving: Int
    let id: Int
    let bathrooms: Double
    let zipCode: Int
    var identifier: String?
    var lat: Double?
    var long: Double?
    
    enum CodingKeys: String, CodingKey {
        case price = "price"
        case bedrooms = "bedrooms"
        case bathrooms = "bathrooms"
        case sqftLiving = "sqft_living"
        case id = "id"
        case zipCode = "zipcode"
        case identifier = ""
        case lat = "lat"
        case long = "long"
    }
}

class NetworkService: NSObject {
    
    let url = "http://192.168.43.58:5000/properties?"
    
    func getResponse<T: Codable>(request: URLRequest, handler: @escaping (_ result: [T]?, _ error: Error?) -> Void) {
        
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let decoder = JSONDecoder()
                let propertyModel = try? decoder.decode([T].self, from: data)
                for property in (propertyModel as? [Property] ?? []) {
                    property.identifier = "\(property.id)"
                }
                 print("PropModel:",propertyModel)
                handler(propertyModel, nil)
            } catch {
                print("error:")
            }
        }
        
        return
        
        let task = URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil,let usableData = data {
                let decoder = JSONDecoder()
                let propertyModel = try? decoder.decode([T].self, from: usableData)
                for property in (propertyModel as? [Property] ?? []) {
                    property.identifier = "\(property.id)"
                }
                handler(propertyModel, nil)
            } else {
                print("error:",error?.localizedDescription)
            }
        }
        task.resume()
    }

    func nsdataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    func getRecommendedProperties(owned: Bool, income: String, handler: @escaping (_ result: [Property]?, _ error: Error?) -> Void) {
        var icat = 1
        let incomeInt = income.count > 0 ? Int(Double(income)!) : 0
        var loanAmt: Int = 7000
        if incomeInt >= 250000 {
            loanAmt = 70000
            icat = 3
        } else if incomeInt >= 150000 {
            icat = 2
        }
        
        
        let completeURL = self.url + "homeOwnershipCat=1&annualInc=\(income)&loanAmount=\(loanAmt)&termCat=1&incomeCat=\(icat)&pageNo=1&pageSize=10"
        print("url:",completeURL)
        var request = URLRequest(url:URL(string: completeURL)!)
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.getResponse(request: request) { (data, error) in
            handler(data, error)
        }
    }
}
