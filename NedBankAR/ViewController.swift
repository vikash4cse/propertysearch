//
//  ViewController.swift
//  CoreML in ARKit
//
//  Created by Hanley Weng on 14/7/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import Vision
import KUIPopOver
import LSDialogViewController

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet var listView: UIView!
    var owned: Bool = false
    var maxIncome: String = ""

    @IBOutlet weak var debugTextView: UITextView!

    let bubbleDepth : Float = 0.01
    var latestPrediction : String = "…"
    
    var visionRequests = [VNRequest]()
    let dispatchQueueML = DispatchQueue(label: "com.vikash.dispatchqueueml")
    var isViewPresented = false
    
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 50.0
    private var fillColor = UIColor(red: 252.0/255.0, green: 161.0/255.0, blue: 52.0/255.0, alpha: 1.0)
    let customView = CustomPopOverView(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: 300.0, height: 120.0)))

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.listView.isHidden = true
        sceneView.delegate = self
        sceneView.showsStatistics = true
        
        let scene = SCNScene()
        sceneView.scene = scene
        sceneView.autoenablesDefaultLighting = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gestureRecognize:)))
        self.listView.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.handlePropertyTap(gestureRecognize:)))
        self.customView.addGestureRecognizer(tapGesture1)

        guard let selectedModel = try? VNCoreMLModel(for: ImageClassifier().model) else {
            fatalError("Could not load model.")
        }
        
        let classificationRequest = VNCoreMLRequest(model: selectedModel, completionHandler: classificationCompleteHandler)
        classificationRequest.imageCropAndScaleOption = VNImageCropAndScaleOption.centerCrop
        visionRequests = [classificationRequest]
        loopCoreMLUpdate()
        self.showShadow()
        
        let dialogViewController: CustomDialogViewController = CustomDialogViewController(nibName:"CustomDialog", bundle: nil)
        dialogViewController.delegate = self
        //self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.fadeInOut, completion: { () -> Void in })
        self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.fadeInOut, backgroundViewType: LSDialogBackgroundViewType.none, dismissButtonEnabled: false, completion: nil)
    }
    
    func showShadow() {
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: self.listView.bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 3
            self.listView.layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func dismissDialog() {
        dismissDialogViewController(LSAnimationPattern.fadeInOut)
    }
    
    func userMaxIncome(_ income: String, isReadyToOwn: Bool) {
        self.maxIncome = income
        self.owned = isReadyToOwn
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @objc func handlePropertyTap(gestureRecognize: UITapGestureRecognizer) {
        
        customView.dismissPopover(animated: false)
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetailViewController") as? PropertyDetailViewController {
            viewController.incomeRange = maxIncome
            viewController.owned = owned
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    @objc func handleTap(gestureRecognize: UITapGestureRecognizer) {
        
        customView.showPopover(sourceView: self.listView)
    }
    
    func loopCoreMLUpdate() {
        dispatchQueueML.async {
            self.updateCoreML()
            self.loopCoreMLUpdate()
        }
    }
    
    func classificationCompleteHandler(request: VNRequest, error: Error?) {

        if error != nil {
            print("Error: " + (error?.localizedDescription)!)
            return
        }
        guard let observations = request.results else {
            print("No results")
            return
        }
        
        let classifications = observations[0...1].compactMap({ $0 as? VNClassificationObservation })
            .map({ "\($0.identifier) \(String(format:"- %.2f", $0.confidence))" })
            .joined(separator: "\n")
        DispatchQueue.main.async {
            var debugText:String = ""
            debugText += classifications
            self.debugTextView.text = debugText
            var objectName:String = "…"
            objectName = classifications.components(separatedBy: "-")[0]
            objectName = objectName.components(separatedBy: ",")[0]
            self.latestPrediction = objectName
            if !self.isViewPresented {
                self.isViewPresented = !self.isViewPresented
                self.listView.isHidden = false
            }
        }
    }
    
    func updateCoreML() {

        let pixbuff : CVPixelBuffer? = (sceneView.session.currentFrame?.capturedImage)
        if pixbuff == nil { return }
        let ciImage = CIImage(cvPixelBuffer: pixbuff!)
        let imageRequestHandler = VNImageRequestHandler(ciImage: ciImage, options: [:])
        do {
            try imageRequestHandler.perform(self.visionRequests)
        } catch {
            print(error)
        }
        
    }
}

class CustomPopOverView: UIView, KUIPopOverUsable {
    
    let tempView: PropertyPopover = UIView.fromNib()
    var contentSize: CGSize {
        return CGSize(width: 320.0, height: 120.0)
    }
    
    var popOverBackgroundColor: UIColor? {
        return .white
    }
    
    var arrowDirection: UIPopoverArrowDirection {
        return .down
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(tempView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
