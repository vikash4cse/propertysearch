//
//  PropertyDetailViewController.swift
//  NedBankAR
//
//  Created by Nagarro on 22/07/19.
//  Copyright © 2019 CompanyName. All rights reserved.
//

import UIKit
import LSDialogViewController
import FaveButton
import CoreLocation

class CollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var price: UILabel!
    
}

class PropertyDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, FaveButtonDelegate {
    
    @IBOutlet weak var favButton: FaveButton!
    
    var owned: Bool = false
    var incomeRange: String = ""
    let networkService = NetworkService()
    var properties: [Property]?
    
    let images = ["1","2","3","4","5","6","7","8","9","10"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        favButton.delegate = self
        networkService.getRecommendedProperties(owned: owned, income: self.incomeRange) { (properties, error) in
            self.properties = properties
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
        let exists = DataManager.shared.geoLocations.filter { $0.identifier == "1274500060" }
        
        if !exists.isEmpty { favButton.isSelected = true; favButton.isEnabled = true; favButton.isUserInteractionEnabled = false }
    }
    
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        
        let locationManager = CLLocationManager()
        let radius = 10.0
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let coordinate = CLLocationCoordinate2DMake(-33.643099, 19.447535)
        let identifier = "1274500060"
        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier)
        DataManager.shared.geoLocations.append(geotification)
        (UIApplication.shared.delegate as? AppDelegate)?.startMonitoring(geotification: geotification)
        updateGeotifications()
    }
    
    func updateGeotifications() {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(DataManager.shared.geoLocations)
            UserDefaults.standard.set(data, forKey: PreferencesKeys.savedItems)
        } catch {
            print("error encoding geotifications")
        }
    }
    
    let reuseIdentifier = "collectionViewCellId"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        cell.imageView.image = UIImage(named: images[indexPath.row])
        guard let property = properties?[indexPath.item] else { return cell }
        
        cell.price.text = "R" + "\(property.price)"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "RecommendedPropertyViewController") as? RecommendedPropertyViewController {
            viewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
             viewController.property = properties?[indexPath.item]
            self.presentDialogViewController(viewController, animationPattern: LSAnimationPattern.fadeInOut, backgroundViewType: LSDialogBackgroundViewType.none, dismissButtonEnabled: false, completion: nil)
            viewController.backgroundImageView.image = UIImage(named: images[indexPath.row])
            viewController.customDelegate = self
        }
    }
    
    func dismissDialog() {
        dismissDialogViewController(LSAnimationPattern.fadeInOut)
    }
    
}
