//
//  PropertyPopover.swift
//  NedbankAR
//
//  Created by Rupender on 04/07/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import UIKit

protocol OverlayDelegate: class {
    func tapReceived()
}

class PropertyPopover: UIView {

    @IBOutlet weak var buttonTapped: UIButton!
    weak var handler: OverlayDelegate?
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.handler?.tapReceived()
        }
    }
}
