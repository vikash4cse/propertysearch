
import UIKit
import TTRangeSlider

class CustomDialogViewController: UIViewController, TTRangeSliderDelegate {
    
    var delegate: ViewController?
    @IBOutlet weak var currencyRange: TTRangeSlider!
    @IBOutlet weak var checkBoxButton: CheckBox!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        view.bounds.size.height = 320
        view.bounds.size.width = UIScreen.main.bounds.size.width * 0.8
        
        self.currencyRange.delegate = self
        self.currencyRange.minValue = 10000
        self.currencyRange.maxValue = 2500000
        self.currencyRange.selectedMinimum = 10000
        self.currencyRange.selectedMaximum = 2500000
        self.currencyRange.handleColor = UIColor(displayP3Red: (0.0/255.0), green: (150.0/255.0), blue: (57.0/255.0), alpha: 1.0)
        self.currencyRange.handleDiameter = 30;
        self.currencyRange.selectedHandleDiameterMultiplier = 1.3;
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        
        delegate?.userMaxIncome("\(currencyRange.selectedMaximum)", isReadyToOwn: checkBoxButton.isChecked)
    }
    
    @IBAction func closeButton(_ sender: AnyObject) {
        delegate?.dismissDialog()
    }
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
    }
}

class CheckBox: UIButton {
    // Images
    let checkedImage = UIImage(named: "Checked")! as UIImage
    let uncheckedImage = UIImage(named: "Unchecked")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: .normal)
            } else {
                self.setImage(uncheckedImage, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        //self.addTarget(self, action: #selector(buttonClicked:), for: UIControlEvents.touchUpInside)
        self.addTarget(self, action: #selector(buttonClicked), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
