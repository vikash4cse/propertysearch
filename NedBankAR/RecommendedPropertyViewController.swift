//
//  RecommendedPropertyViewController.swift
//  NedBankAR
//
//  Created by Nagarro on 13/08/19.
//  Copyright © 2019 CompanyName. All rights reserved.
//

import UIKit
import FaveButton
import CoreLocation

class RecommendedPropertyViewController: UIViewController, FaveButtonDelegate {

    var customDelegate: PropertyDetailViewController?
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var roomsLabel: UILabel!
    @IBOutlet weak var sqftLivingLabel: UILabel!
    @IBOutlet weak var zipCodeLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var favButton: FaveButton!
    var property: Property?

    @IBAction func dismiss(_ sender: Any) {
        
        customDelegate?.dismissDialog()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favButton.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.populateData()
        print("propiD:",property!.identifier)
        let exists = DataManager.shared.geoLocations.filter {
//            print("ID:",$0.identifier)
            $0.identifier == property!.identifier
            
        }
        
        if !exists.isEmpty { favButton.isSelected = true; favButton.isEnabled = true; favButton.isUserInteractionEnabled = false }
    }
    
    func populateData() {
        
        priceLabel.text =  "Price : \(property?.price ?? 0)"
        roomsLabel.text = "Bedrooms : \(property?.bedrooms ?? 0), Bathroooms : \(property?.bathrooms ?? 0.0)"
        sqftLivingLabel.text = "Living Area : \(property?.sqftLiving ?? 0)"
        zipCodeLabel.text = "Zipcode : \(property?.zipCode ?? 0)"
        
    }
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        let locationManager = CLLocationManager()
        let radius = 10.0
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let coordinate = CLLocationCoordinate2DMake(property!.lat!, property!.long!)
        let identifier = (property?.identifier)!
        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier)
        DataManager.shared.geoLocations.append(geotification)
        (UIApplication.shared.delegate as? AppDelegate)?.startMonitoring(geotification: geotification)
        updateGeotifications()
    }
    
    func updateGeotifications() {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(DataManager.shared.geoLocations)
            UserDefaults.standard.set(data, forKey: PreferencesKeys.savedItems)
        } catch {
            print("error encoding geotifications")
        }
    }
}
